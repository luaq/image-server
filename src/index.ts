import express from "express";
import canvas = require("canvas");
import * as path from "path";


const app  = express();
const port = 80;

// register the custom font
canvas.registerFont(path.join(process.cwd(), "ProductSans-Regular.ttf"), { family: "Product Sans" });

function draw(text: string, width = 400, height = width * 1.5) {
  const image = canvas.createCanvas(width, height);
  const ctx   = image.getContext("2d");

  // replace pluses with spaces
  text = text.replace(/\+/g, " ");

  /* break the text by every 24 characters (word wrap)
    using fancy regex patterns ;o (thanks stackoverflow) 
    https://stackoverflow.com/questions/14484787/wrap-text-in-javascript */
  text = text.replace(/(?![^\n]{1,24}$)([^\n]{1,24})\s/g, "$1\n");

  ctx.fillStyle = "#0e0e0e";
  ctx.fillRect(0, 0, width, height);

  ctx.fillStyle = "#ffffff";
  ctx.textAlign = "center";
  ctx.font = "32px 'Product Sans'";
  
  // make sure it's centered vertically as well
  const textHeight = parseInt(ctx.font) * (text.split("\n").length) / 2;

  ctx.fillText(text, width / 2, height / 2 - textHeight);

  // render the watermark

  const watermark = "generator by luaq.";
  const padding   = 10;

  ctx.font = "16px 'Product Sans'";
  ctx.fillStyle = "rgba(255, 255, 255, .5);"
  ctx.textAlign = "right";

  ctx.fillText(watermark, width - padding, height - padding);

  // return the canvas to be
  // rendered to the user as a png
  return image;
}

app.get("/:text", (req, res) => {
  const width = req.query.width;
  const height = req.query.height;

  // pipe the response with the content-type
  res.contentType("png");
  draw(req.params.text, width, height).createPNGStream().pipe(res);
});


app.listen(port, () => {
  console.log("server started.")
});
